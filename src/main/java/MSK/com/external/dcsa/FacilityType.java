/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package MSK.com.external.dcsa;
@org.apache.avro.specific.AvroGenerated
public enum FacilityType implements org.apache.avro.generic.GenericEnumSymbol<FacilityType> {
  BOCR, CLOC, COFS, COYA, OFFD, DEPO, INTE, POTE, PBPL, BRTH  ;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"enum\",\"name\":\"FacilityType\",\"namespace\":\"MSK.com.external.dcsa\",\"symbols\":[\"BOCR\",\"CLOC\",\"COFS\",\"COYA\",\"OFFD\",\"DEPO\",\"INTE\",\"POTE\",\"PBPL\",\"BRTH\"]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
}
