/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package MSK.com.external.dcsa;
@org.apache.avro.specific.AvroGenerated
public enum ShipmentInformationType implements org.apache.avro.generic.GenericEnumSymbol<ShipmentInformationType> {
  BOK, SHI, VGM, SRM, TRD, ARN  ;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"enum\",\"name\":\"ShipmentInformationType\",\"namespace\":\"MSK.com.external.dcsa\",\"symbols\":[\"BOK\",\"SHI\",\"VGM\",\"SRM\",\"TRD\",\"ARN\"]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
}
