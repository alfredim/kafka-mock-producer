package MSK.com.external.dcsa;
@org.apache.avro.specific.AvroGenerated
public enum BadKey implements org.apache.avro.generic.GenericEnumSymbol<BadKey> {
  MAEU, SAFM, MCCQ, SEJJ, SEAU  ;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"enum\",\"name\":\"BadKey\",\"namespace\":\"MSK.com.external.dcsa\",\"symbols\":[\"MAEU\",\"SAFM\",\"MCCQ\",\"SEJJ\",\"SEAU\"]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
}
