/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package MSK.com.external.dcsa;
@org.apache.avro.specific.AvroGenerated
public enum TransPortMode implements org.apache.avro.generic.GenericEnumSymbol<TransPortMode> {
  VESSEL, RAIL, TRUCK, BARGE  ;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"enum\",\"name\":\"TransPortMode\",\"namespace\":\"MSK.com.external.dcsa\",\"symbols\":[\"VESSEL\",\"RAIL\",\"TRUCK\",\"BARGE\"]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
}
