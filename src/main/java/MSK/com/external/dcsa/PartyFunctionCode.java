/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package MSK.com.external.dcsa;
@org.apache.avro.specific.AvroGenerated
public enum PartyFunctionCode implements org.apache.avro.generic.GenericEnumSymbol<PartyFunctionCode> {
  OS, CN, N1, N2, NI, COW, COX, DDR, DDS  ;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"enum\",\"name\":\"PartyFunctionCode\",\"namespace\":\"MSK.com.external.dcsa\",\"symbols\":[\"OS\",\"CN\",\"N1\",\"N2\",\"NI\",\"COW\",\"COX\",\"DDR\",\"DDS\"]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
}
