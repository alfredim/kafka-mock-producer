/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package MSK.com.gems;

import org.apache.avro.generic.GenericArray;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.util.Utf8;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@org.apache.avro.specific.AvroGenerated
public class CargoShipType extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = -4019557658902751273L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"CargoShipType\",\"namespace\":\"MSK.com.gems\",\"fields\":[{\"name\":\"cargoConditionings\",\"type\":[\"null\",{\"type\":\"record\",\"name\":\"CargoConditioningsType\",\"fields\":[{\"name\":\"cargoConditioning\",\"type\":[\"null\",{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"CargoConditioningType\",\"fields\":[{\"name\":\"atmosphereControlRequired\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"co2Percentage\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"co2PercentageRequired\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"coldTreatmentRequired\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"containerConditioningType\",\"type\":[\"null\",{\"type\":\"record\",\"name\":\"ContainerConditioningTypeType\",\"fields\":[{\"name\":\"typ\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"value\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]}]}]},{\"name\":\"containerTypeRequired\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"dataLoggerRequired\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"drainHolesOpen\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"ethyleneFilterCount\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"fishloaderService\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"freezerStuffContainerRequired\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"gensetFromOrigin\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"gensetNumberDestination\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"gensetNumberOrigin\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"gensetToDestination\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"highValueCargo\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"hotStuffing\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"humidityPerCentage\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"humidityRequired\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"individualQuickFrozen\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"manualReviewCompleted\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"measureUnitTemperature\",\"type\":[\"null\",{\"type\":\"record\",\"name\":\"MeasureUnitTemperatureType\",\"fields\":[{\"name\":\"typ\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"value\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]}]}]},{\"name\":\"measureUnitVentilation\",\"type\":[\"null\",{\"type\":\"record\",\"name\":\"MeasureUnitVentilationType\",\"fields\":[{\"name\":\"typ\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"value\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]}]}]},{\"name\":\"multipleSetpointsRequired\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"o2Percentage\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"o2PercentageRequired\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"preCoolingRequired\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"probesCount\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"reeferTemplateVersion\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"reeferValidationStatus\",\"type\":[\"null\",{\"type\":\"record\",\"name\":\"ReeferValidationStatusType\",\"fields\":[{\"name\":\"typ\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"value\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]}]}]},{\"name\":\"sortieContainerRequired\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"temperature\",\"type\":[\"null\",{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"TemperatureType\",\"fields\":[{\"name\":\"typ\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"value\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]}]}}]},{\"name\":\"temperatureLo\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"temperatureRequired\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"timePropertiesValidated\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"ventilationRequired\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"ventilationVolume\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"ventilationVolumeCbmHr\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]}]}}]}]}]}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<CargoShipType> ENCODER =
      new BinaryMessageEncoder<CargoShipType>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<CargoShipType> DECODER =
      new BinaryMessageDecoder<CargoShipType>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageEncoder instance used by this class.
   * @return the message encoder used by this class
   */
  public static BinaryMessageEncoder<CargoShipType> getEncoder() {
    return ENCODER;
  }

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   * @return the message decoder used by this class
   */
  public static BinaryMessageDecoder<CargoShipType> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   * @return a BinaryMessageDecoder instance for this class backed by the given SchemaStore
   */
  public static BinaryMessageDecoder<CargoShipType> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<CargoShipType>(MODEL$, SCHEMA$, resolver);
  }

  /**
   * Serializes this CargoShipType to a ByteBuffer.
   * @return a buffer holding the serialized data for this instance
   * @throws java.io.IOException if this instance could not be serialized
   */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /**
   * Deserializes a CargoShipType from a ByteBuffer.
   * @param b a byte buffer holding serialized data for an instance of this class
   * @return a CargoShipType instance decoded from the given buffer
   * @throws java.io.IOException if the given bytes could not be deserialized into an instance of this class
   */
  public static CargoShipType fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

   private MSK.com.gems.CargoConditioningsType cargoConditionings;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public CargoShipType() {}

  /**
   * All-args constructor.
   * @param cargoConditionings The new value for cargoConditionings
   */
  public CargoShipType(MSK.com.gems.CargoConditioningsType cargoConditionings) {
    this.cargoConditionings = cargoConditionings;
  }

  public org.apache.avro.specific.SpecificData getSpecificData() { return MODEL$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return cargoConditionings;
    default: throw new IndexOutOfBoundsException("Invalid index: " + field$);
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: cargoConditionings = (MSK.com.gems.CargoConditioningsType)value$; break;
    default: throw new IndexOutOfBoundsException("Invalid index: " + field$);
    }
  }

  /**
   * Gets the value of the 'cargoConditionings' field.
   * @return The value of the 'cargoConditionings' field.
   */
  public MSK.com.gems.CargoConditioningsType getCargoConditionings() {
    return cargoConditionings;
  }


  /**
   * Sets the value of the 'cargoConditionings' field.
   * @param value the value to set.
   */
  public void setCargoConditionings(MSK.com.gems.CargoConditioningsType value) {
    this.cargoConditionings = value;
  }

  /**
   * Creates a new CargoShipType RecordBuilder.
   * @return A new CargoShipType RecordBuilder
   */
  public static MSK.com.gems.CargoShipType.Builder newBuilder() {
    return new MSK.com.gems.CargoShipType.Builder();
  }

  /**
   * Creates a new CargoShipType RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new CargoShipType RecordBuilder
   */
  public static MSK.com.gems.CargoShipType.Builder newBuilder(MSK.com.gems.CargoShipType.Builder other) {
    if (other == null) {
      return new MSK.com.gems.CargoShipType.Builder();
    } else {
      return new MSK.com.gems.CargoShipType.Builder(other);
    }
  }

  /**
   * Creates a new CargoShipType RecordBuilder by copying an existing CargoShipType instance.
   * @param other The existing instance to copy.
   * @return A new CargoShipType RecordBuilder
   */
  public static MSK.com.gems.CargoShipType.Builder newBuilder(MSK.com.gems.CargoShipType other) {
    if (other == null) {
      return new MSK.com.gems.CargoShipType.Builder();
    } else {
      return new MSK.com.gems.CargoShipType.Builder(other);
    }
  }

  /**
   * RecordBuilder for CargoShipType instances.
   */
  @org.apache.avro.specific.AvroGenerated
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<CargoShipType>
    implements org.apache.avro.data.RecordBuilder<CargoShipType> {

    private MSK.com.gems.CargoConditioningsType cargoConditionings;
    private MSK.com.gems.CargoConditioningsType.Builder cargoConditioningsBuilder;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(MSK.com.gems.CargoShipType.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.cargoConditionings)) {
        this.cargoConditionings = data().deepCopy(fields()[0].schema(), other.cargoConditionings);
        fieldSetFlags()[0] = other.fieldSetFlags()[0];
      }
      if (other.hasCargoConditioningsBuilder()) {
        this.cargoConditioningsBuilder = MSK.com.gems.CargoConditioningsType.newBuilder(other.getCargoConditioningsBuilder());
      }
    }

    /**
     * Creates a Builder by copying an existing CargoShipType instance
     * @param other The existing instance to copy.
     */
    private Builder(MSK.com.gems.CargoShipType other) {
      super(SCHEMA$);
      if (isValidValue(fields()[0], other.cargoConditionings)) {
        this.cargoConditionings = data().deepCopy(fields()[0].schema(), other.cargoConditionings);
        fieldSetFlags()[0] = true;
      }
      this.cargoConditioningsBuilder = null;
    }

    /**
      * Gets the value of the 'cargoConditionings' field.
      * @return The value.
      */
    public MSK.com.gems.CargoConditioningsType getCargoConditionings() {
      return cargoConditionings;
    }


    /**
      * Sets the value of the 'cargoConditionings' field.
      * @param value The value of 'cargoConditionings'.
      * @return This builder.
      */
    public MSK.com.gems.CargoShipType.Builder setCargoConditionings(MSK.com.gems.CargoConditioningsType value) {
      validate(fields()[0], value);
      this.cargoConditioningsBuilder = null;
      this.cargoConditionings = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'cargoConditionings' field has been set.
      * @return True if the 'cargoConditionings' field has been set, false otherwise.
      */
    public boolean hasCargoConditionings() {
      return fieldSetFlags()[0];
    }

    /**
     * Gets the Builder instance for the 'cargoConditionings' field and creates one if it doesn't exist yet.
     * @return This builder.
     */
    public MSK.com.gems.CargoConditioningsType.Builder getCargoConditioningsBuilder() {
      if (cargoConditioningsBuilder == null) {
        if (hasCargoConditionings()) {
          setCargoConditioningsBuilder(MSK.com.gems.CargoConditioningsType.newBuilder(cargoConditionings));
        } else {
          setCargoConditioningsBuilder(MSK.com.gems.CargoConditioningsType.newBuilder());
        }
      }
      return cargoConditioningsBuilder;
    }

    /**
     * Sets the Builder instance for the 'cargoConditionings' field
     * @param value The builder instance that must be set.
     * @return This builder.
     */

    public MSK.com.gems.CargoShipType.Builder setCargoConditioningsBuilder(MSK.com.gems.CargoConditioningsType.Builder value) {
      clearCargoConditionings();
      cargoConditioningsBuilder = value;
      return this;
    }

    /**
     * Checks whether the 'cargoConditionings' field has an active Builder instance
     * @return True if the 'cargoConditionings' field has an active Builder instance
     */
    public boolean hasCargoConditioningsBuilder() {
      return cargoConditioningsBuilder != null;
    }

    /**
      * Clears the value of the 'cargoConditionings' field.
      * @return This builder.
      */
    public MSK.com.gems.CargoShipType.Builder clearCargoConditionings() {
      cargoConditionings = null;
      cargoConditioningsBuilder = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public CargoShipType build() {
      try {
        CargoShipType record = new CargoShipType();
        if (cargoConditioningsBuilder != null) {
          try {
            record.cargoConditionings = this.cargoConditioningsBuilder.build();
          } catch (org.apache.avro.AvroMissingFieldException e) {
            e.addParentField(record.getSchema().getField("cargoConditionings"));
            throw e;
          }
        } else {
          record.cargoConditionings = fieldSetFlags()[0] ? this.cargoConditionings : (MSK.com.gems.CargoConditioningsType) defaultValue(fields()[0]);
        }
        return record;
      } catch (org.apache.avro.AvroMissingFieldException e) {
        throw e;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<CargoShipType>
    WRITER$ = (org.apache.avro.io.DatumWriter<CargoShipType>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<CargoShipType>
    READER$ = (org.apache.avro.io.DatumReader<CargoShipType>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

  @Override protected boolean hasCustomCoders() { return true; }

  @Override public void customEncode(org.apache.avro.io.Encoder out)
    throws java.io.IOException
  {
    if (this.cargoConditionings == null) {
      out.writeIndex(0);
      out.writeNull();
    } else {
      out.writeIndex(1);
      this.cargoConditionings.customEncode(out);
    }

  }

  @Override public void customDecode(org.apache.avro.io.ResolvingDecoder in)
    throws java.io.IOException
  {
    org.apache.avro.Schema.Field[] fieldOrder = in.readFieldOrderIfDiff();
    if (fieldOrder == null) {
      if (in.readIndex() != 1) {
        in.readNull();
        this.cargoConditionings = null;
      } else {
        if (this.cargoConditionings == null) {
          this.cargoConditionings = new MSK.com.gems.CargoConditioningsType();
        }
        this.cargoConditionings.customDecode(in);
      }

    } else {
      for (int i = 0; i < 1; i++) {
        switch (fieldOrder[i].pos()) {
        case 0:
          if (in.readIndex() != 1) {
            in.readNull();
            this.cargoConditionings = null;
          } else {
            if (this.cargoConditionings == null) {
              this.cargoConditionings = new MSK.com.gems.CargoConditioningsType();
            }
            this.cargoConditionings.customDecode(in);
          }
          break;

        default:
          throw new java.io.IOException("Corrupt ResolvingDecoder.");
        }
      }
    }
  }
}










