/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package MSK.com.gems;

import org.apache.avro.generic.GenericArray;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.util.Utf8;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@org.apache.avro.specific.AvroGenerated
public class ComplianceMessageType extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = -3874955825423254226L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"ComplianceMessageType\",\"namespace\":\"MSK.com.gems\",\"fields\":[{\"name\":\"tag\",\"type\":[\"null\",{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"TAGType\",\"fields\":[{\"name\":\"action\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"value\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]}]}}]}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<ComplianceMessageType> ENCODER =
      new BinaryMessageEncoder<ComplianceMessageType>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<ComplianceMessageType> DECODER =
      new BinaryMessageDecoder<ComplianceMessageType>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageEncoder instance used by this class.
   * @return the message encoder used by this class
   */
  public static BinaryMessageEncoder<ComplianceMessageType> getEncoder() {
    return ENCODER;
  }

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   * @return the message decoder used by this class
   */
  public static BinaryMessageDecoder<ComplianceMessageType> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   * @return a BinaryMessageDecoder instance for this class backed by the given SchemaStore
   */
  public static BinaryMessageDecoder<ComplianceMessageType> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<ComplianceMessageType>(MODEL$, SCHEMA$, resolver);
  }

  /**
   * Serializes this ComplianceMessageType to a ByteBuffer.
   * @return a buffer holding the serialized data for this instance
   * @throws java.io.IOException if this instance could not be serialized
   */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /**
   * Deserializes a ComplianceMessageType from a ByteBuffer.
   * @param b a byte buffer holding serialized data for an instance of this class
   * @return a ComplianceMessageType instance decoded from the given buffer
   * @throws java.io.IOException if the given bytes could not be deserialized into an instance of this class
   */
  public static ComplianceMessageType fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

   private java.util.List<MSK.com.gems.TAGType> tag;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public ComplianceMessageType() {}

  /**
   * All-args constructor.
   * @param tag The new value for tag
   */
  public ComplianceMessageType(java.util.List<MSK.com.gems.TAGType> tag) {
    this.tag = tag;
  }

  public org.apache.avro.specific.SpecificData getSpecificData() { return MODEL$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return tag;
    default: throw new IndexOutOfBoundsException("Invalid index: " + field$);
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: tag = (java.util.List<MSK.com.gems.TAGType>)value$; break;
    default: throw new IndexOutOfBoundsException("Invalid index: " + field$);
    }
  }

  /**
   * Gets the value of the 'tag' field.
   * @return The value of the 'tag' field.
   */
  public java.util.List<MSK.com.gems.TAGType> getTag() {
    return tag;
  }


  /**
   * Sets the value of the 'tag' field.
   * @param value the value to set.
   */
  public void setTag(java.util.List<MSK.com.gems.TAGType> value) {
    this.tag = value;
  }

  /**
   * Creates a new ComplianceMessageType RecordBuilder.
   * @return A new ComplianceMessageType RecordBuilder
   */
  public static MSK.com.gems.ComplianceMessageType.Builder newBuilder() {
    return new MSK.com.gems.ComplianceMessageType.Builder();
  }

  /**
   * Creates a new ComplianceMessageType RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new ComplianceMessageType RecordBuilder
   */
  public static MSK.com.gems.ComplianceMessageType.Builder newBuilder(MSK.com.gems.ComplianceMessageType.Builder other) {
    if (other == null) {
      return new MSK.com.gems.ComplianceMessageType.Builder();
    } else {
      return new MSK.com.gems.ComplianceMessageType.Builder(other);
    }
  }

  /**
   * Creates a new ComplianceMessageType RecordBuilder by copying an existing ComplianceMessageType instance.
   * @param other The existing instance to copy.
   * @return A new ComplianceMessageType RecordBuilder
   */
  public static MSK.com.gems.ComplianceMessageType.Builder newBuilder(MSK.com.gems.ComplianceMessageType other) {
    if (other == null) {
      return new MSK.com.gems.ComplianceMessageType.Builder();
    } else {
      return new MSK.com.gems.ComplianceMessageType.Builder(other);
    }
  }

  /**
   * RecordBuilder for ComplianceMessageType instances.
   */
  @org.apache.avro.specific.AvroGenerated
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<ComplianceMessageType>
    implements org.apache.avro.data.RecordBuilder<ComplianceMessageType> {

    private java.util.List<MSK.com.gems.TAGType> tag;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(MSK.com.gems.ComplianceMessageType.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.tag)) {
        this.tag = data().deepCopy(fields()[0].schema(), other.tag);
        fieldSetFlags()[0] = other.fieldSetFlags()[0];
      }
    }

    /**
     * Creates a Builder by copying an existing ComplianceMessageType instance
     * @param other The existing instance to copy.
     */
    private Builder(MSK.com.gems.ComplianceMessageType other) {
      super(SCHEMA$);
      if (isValidValue(fields()[0], other.tag)) {
        this.tag = data().deepCopy(fields()[0].schema(), other.tag);
        fieldSetFlags()[0] = true;
      }
    }

    /**
      * Gets the value of the 'tag' field.
      * @return The value.
      */
    public java.util.List<MSK.com.gems.TAGType> getTag() {
      return tag;
    }


    /**
      * Sets the value of the 'tag' field.
      * @param value The value of 'tag'.
      * @return This builder.
      */
    public MSK.com.gems.ComplianceMessageType.Builder setTag(java.util.List<MSK.com.gems.TAGType> value) {
      validate(fields()[0], value);
      this.tag = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'tag' field has been set.
      * @return True if the 'tag' field has been set, false otherwise.
      */
    public boolean hasTag() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'tag' field.
      * @return This builder.
      */
    public MSK.com.gems.ComplianceMessageType.Builder clearTag() {
      tag = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ComplianceMessageType build() {
      try {
        ComplianceMessageType record = new ComplianceMessageType();
        record.tag = fieldSetFlags()[0] ? this.tag : (java.util.List<MSK.com.gems.TAGType>) defaultValue(fields()[0]);
        return record;
      } catch (org.apache.avro.AvroMissingFieldException e) {
        throw e;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<ComplianceMessageType>
    WRITER$ = (org.apache.avro.io.DatumWriter<ComplianceMessageType>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<ComplianceMessageType>
    READER$ = (org.apache.avro.io.DatumReader<ComplianceMessageType>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

  @Override protected boolean hasCustomCoders() { return true; }

  @Override public void customEncode(org.apache.avro.io.Encoder out)
    throws java.io.IOException
  {
    if (this.tag == null) {
      out.writeIndex(0);
      out.writeNull();
    } else {
      out.writeIndex(1);
      long size0 = this.tag.size();
      out.writeArrayStart();
      out.setItemCount(size0);
      long actualSize0 = 0;
      for (MSK.com.gems.TAGType e0: this.tag) {
        actualSize0++;
        out.startItem();
        e0.customEncode(out);
      }
      out.writeArrayEnd();
      if (actualSize0 != size0)
        throw new java.util.ConcurrentModificationException("Array-size written was " + size0 + ", but element count was " + actualSize0 + ".");
    }

  }

  @Override public void customDecode(org.apache.avro.io.ResolvingDecoder in)
    throws java.io.IOException
  {
    org.apache.avro.Schema.Field[] fieldOrder = in.readFieldOrderIfDiff();
    if (fieldOrder == null) {
      if (in.readIndex() != 1) {
        in.readNull();
        this.tag = null;
      } else {
        long size0 = in.readArrayStart();
        java.util.List<MSK.com.gems.TAGType> a0 = this.tag;
        if (a0 == null) {
          a0 = new SpecificData.Array<MSK.com.gems.TAGType>((int)size0, SCHEMA$.getField("tag").schema().getTypes().get(1));
          this.tag = a0;
        } else a0.clear();
        SpecificData.Array<MSK.com.gems.TAGType> ga0 = (a0 instanceof SpecificData.Array ? (SpecificData.Array<MSK.com.gems.TAGType>)a0 : null);
        for ( ; 0 < size0; size0 = in.arrayNext()) {
          for ( ; size0 != 0; size0--) {
            MSK.com.gems.TAGType e0 = (ga0 != null ? ga0.peek() : null);
            if (e0 == null) {
              e0 = new MSK.com.gems.TAGType();
            }
            e0.customDecode(in);
            a0.add(e0);
          }
        }
      }

    } else {
      for (int i = 0; i < 1; i++) {
        switch (fieldOrder[i].pos()) {
        case 0:
          if (in.readIndex() != 1) {
            in.readNull();
            this.tag = null;
          } else {
            long size0 = in.readArrayStart();
            java.util.List<MSK.com.gems.TAGType> a0 = this.tag;
            if (a0 == null) {
              a0 = new SpecificData.Array<MSK.com.gems.TAGType>((int)size0, SCHEMA$.getField("tag").schema().getTypes().get(1));
              this.tag = a0;
            } else a0.clear();
            SpecificData.Array<MSK.com.gems.TAGType> ga0 = (a0 instanceof SpecificData.Array ? (SpecificData.Array<MSK.com.gems.TAGType>)a0 : null);
            for ( ; 0 < size0; size0 = in.arrayNext()) {
              for ( ; size0 != 0; size0--) {
                MSK.com.gems.TAGType e0 = (ga0 != null ? ga0.peek() : null);
                if (e0 == null) {
                  e0 = new MSK.com.gems.TAGType();
                }
                e0.customDecode(in);
                a0.add(e0);
              }
            }
          }
          break;

        default:
          throw new java.io.IOException("Corrupt ResolvingDecoder.");
        }
      }
    }
  }
}










