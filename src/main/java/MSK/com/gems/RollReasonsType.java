/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package MSK.com.gems;

import org.apache.avro.generic.GenericArray;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.util.Utf8;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@org.apache.avro.specific.AvroGenerated
public class RollReasonsType extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = -3954032759983564913L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"RollReasonsType\",\"namespace\":\"MSK.com.gems\",\"fields\":[{\"name\":\"code\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"reasonCategory\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"reasonCustomerView\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"reasonDescription\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"reasonDetailsCustomerView\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"routeUpdateReasonAction\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"routeUpdateReasonDetails\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"routeUpdateReasonTrigger\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"updateTimeStamp\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<RollReasonsType> ENCODER =
      new BinaryMessageEncoder<RollReasonsType>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<RollReasonsType> DECODER =
      new BinaryMessageDecoder<RollReasonsType>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageEncoder instance used by this class.
   * @return the message encoder used by this class
   */
  public static BinaryMessageEncoder<RollReasonsType> getEncoder() {
    return ENCODER;
  }

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   * @return the message decoder used by this class
   */
  public static BinaryMessageDecoder<RollReasonsType> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   * @return a BinaryMessageDecoder instance for this class backed by the given SchemaStore
   */
  public static BinaryMessageDecoder<RollReasonsType> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<RollReasonsType>(MODEL$, SCHEMA$, resolver);
  }

  /**
   * Serializes this RollReasonsType to a ByteBuffer.
   * @return a buffer holding the serialized data for this instance
   * @throws java.io.IOException if this instance could not be serialized
   */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /**
   * Deserializes a RollReasonsType from a ByteBuffer.
   * @param b a byte buffer holding serialized data for an instance of this class
   * @return a RollReasonsType instance decoded from the given buffer
   * @throws java.io.IOException if the given bytes could not be deserialized into an instance of this class
   */
  public static RollReasonsType fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

   private java.lang.String code;
   private java.lang.String reasonCategory;
   private java.lang.String reasonCustomerView;
   private java.lang.String reasonDescription;
   private java.lang.String reasonDetailsCustomerView;
   private java.lang.String routeUpdateReasonAction;
   private java.lang.String routeUpdateReasonDetails;
   private java.lang.String routeUpdateReasonTrigger;
   private java.lang.String updateTimeStamp;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public RollReasonsType() {}

  /**
   * All-args constructor.
   * @param code The new value for code
   * @param reasonCategory The new value for reasonCategory
   * @param reasonCustomerView The new value for reasonCustomerView
   * @param reasonDescription The new value for reasonDescription
   * @param reasonDetailsCustomerView The new value for reasonDetailsCustomerView
   * @param routeUpdateReasonAction The new value for routeUpdateReasonAction
   * @param routeUpdateReasonDetails The new value for routeUpdateReasonDetails
   * @param routeUpdateReasonTrigger The new value for routeUpdateReasonTrigger
   * @param updateTimeStamp The new value for updateTimeStamp
   */
  public RollReasonsType(java.lang.String code, java.lang.String reasonCategory, java.lang.String reasonCustomerView, java.lang.String reasonDescription, java.lang.String reasonDetailsCustomerView, java.lang.String routeUpdateReasonAction, java.lang.String routeUpdateReasonDetails, java.lang.String routeUpdateReasonTrigger, java.lang.String updateTimeStamp) {
    this.code = code;
    this.reasonCategory = reasonCategory;
    this.reasonCustomerView = reasonCustomerView;
    this.reasonDescription = reasonDescription;
    this.reasonDetailsCustomerView = reasonDetailsCustomerView;
    this.routeUpdateReasonAction = routeUpdateReasonAction;
    this.routeUpdateReasonDetails = routeUpdateReasonDetails;
    this.routeUpdateReasonTrigger = routeUpdateReasonTrigger;
    this.updateTimeStamp = updateTimeStamp;
  }

  public org.apache.avro.specific.SpecificData getSpecificData() { return MODEL$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return code;
    case 1: return reasonCategory;
    case 2: return reasonCustomerView;
    case 3: return reasonDescription;
    case 4: return reasonDetailsCustomerView;
    case 5: return routeUpdateReasonAction;
    case 6: return routeUpdateReasonDetails;
    case 7: return routeUpdateReasonTrigger;
    case 8: return updateTimeStamp;
    default: throw new IndexOutOfBoundsException("Invalid index: " + field$);
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: code = value$ != null ? value$.toString() : null; break;
    case 1: reasonCategory = value$ != null ? value$.toString() : null; break;
    case 2: reasonCustomerView = value$ != null ? value$.toString() : null; break;
    case 3: reasonDescription = value$ != null ? value$.toString() : null; break;
    case 4: reasonDetailsCustomerView = value$ != null ? value$.toString() : null; break;
    case 5: routeUpdateReasonAction = value$ != null ? value$.toString() : null; break;
    case 6: routeUpdateReasonDetails = value$ != null ? value$.toString() : null; break;
    case 7: routeUpdateReasonTrigger = value$ != null ? value$.toString() : null; break;
    case 8: updateTimeStamp = value$ != null ? value$.toString() : null; break;
    default: throw new IndexOutOfBoundsException("Invalid index: " + field$);
    }
  }

  /**
   * Gets the value of the 'code' field.
   * @return The value of the 'code' field.
   */
  public java.lang.String getCode() {
    return code;
  }


  /**
   * Sets the value of the 'code' field.
   * @param value the value to set.
   */
  public void setCode(java.lang.String value) {
    this.code = value;
  }

  /**
   * Gets the value of the 'reasonCategory' field.
   * @return The value of the 'reasonCategory' field.
   */
  public java.lang.String getReasonCategory() {
    return reasonCategory;
  }


  /**
   * Sets the value of the 'reasonCategory' field.
   * @param value the value to set.
   */
  public void setReasonCategory(java.lang.String value) {
    this.reasonCategory = value;
  }

  /**
   * Gets the value of the 'reasonCustomerView' field.
   * @return The value of the 'reasonCustomerView' field.
   */
  public java.lang.String getReasonCustomerView() {
    return reasonCustomerView;
  }


  /**
   * Sets the value of the 'reasonCustomerView' field.
   * @param value the value to set.
   */
  public void setReasonCustomerView(java.lang.String value) {
    this.reasonCustomerView = value;
  }

  /**
   * Gets the value of the 'reasonDescription' field.
   * @return The value of the 'reasonDescription' field.
   */
  public java.lang.String getReasonDescription() {
    return reasonDescription;
  }


  /**
   * Sets the value of the 'reasonDescription' field.
   * @param value the value to set.
   */
  public void setReasonDescription(java.lang.String value) {
    this.reasonDescription = value;
  }

  /**
   * Gets the value of the 'reasonDetailsCustomerView' field.
   * @return The value of the 'reasonDetailsCustomerView' field.
   */
  public java.lang.String getReasonDetailsCustomerView() {
    return reasonDetailsCustomerView;
  }


  /**
   * Sets the value of the 'reasonDetailsCustomerView' field.
   * @param value the value to set.
   */
  public void setReasonDetailsCustomerView(java.lang.String value) {
    this.reasonDetailsCustomerView = value;
  }

  /**
   * Gets the value of the 'routeUpdateReasonAction' field.
   * @return The value of the 'routeUpdateReasonAction' field.
   */
  public java.lang.String getRouteUpdateReasonAction() {
    return routeUpdateReasonAction;
  }


  /**
   * Sets the value of the 'routeUpdateReasonAction' field.
   * @param value the value to set.
   */
  public void setRouteUpdateReasonAction(java.lang.String value) {
    this.routeUpdateReasonAction = value;
  }

  /**
   * Gets the value of the 'routeUpdateReasonDetails' field.
   * @return The value of the 'routeUpdateReasonDetails' field.
   */
  public java.lang.String getRouteUpdateReasonDetails() {
    return routeUpdateReasonDetails;
  }


  /**
   * Sets the value of the 'routeUpdateReasonDetails' field.
   * @param value the value to set.
   */
  public void setRouteUpdateReasonDetails(java.lang.String value) {
    this.routeUpdateReasonDetails = value;
  }

  /**
   * Gets the value of the 'routeUpdateReasonTrigger' field.
   * @return The value of the 'routeUpdateReasonTrigger' field.
   */
  public java.lang.String getRouteUpdateReasonTrigger() {
    return routeUpdateReasonTrigger;
  }


  /**
   * Sets the value of the 'routeUpdateReasonTrigger' field.
   * @param value the value to set.
   */
  public void setRouteUpdateReasonTrigger(java.lang.String value) {
    this.routeUpdateReasonTrigger = value;
  }

  /**
   * Gets the value of the 'updateTimeStamp' field.
   * @return The value of the 'updateTimeStamp' field.
   */
  public java.lang.String getUpdateTimeStamp() {
    return updateTimeStamp;
  }


  /**
   * Sets the value of the 'updateTimeStamp' field.
   * @param value the value to set.
   */
  public void setUpdateTimeStamp(java.lang.String value) {
    this.updateTimeStamp = value;
  }

  /**
   * Creates a new RollReasonsType RecordBuilder.
   * @return A new RollReasonsType RecordBuilder
   */
  public static MSK.com.gems.RollReasonsType.Builder newBuilder() {
    return new MSK.com.gems.RollReasonsType.Builder();
  }

  /**
   * Creates a new RollReasonsType RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new RollReasonsType RecordBuilder
   */
  public static MSK.com.gems.RollReasonsType.Builder newBuilder(MSK.com.gems.RollReasonsType.Builder other) {
    if (other == null) {
      return new MSK.com.gems.RollReasonsType.Builder();
    } else {
      return new MSK.com.gems.RollReasonsType.Builder(other);
    }
  }

  /**
   * Creates a new RollReasonsType RecordBuilder by copying an existing RollReasonsType instance.
   * @param other The existing instance to copy.
   * @return A new RollReasonsType RecordBuilder
   */
  public static MSK.com.gems.RollReasonsType.Builder newBuilder(MSK.com.gems.RollReasonsType other) {
    if (other == null) {
      return new MSK.com.gems.RollReasonsType.Builder();
    } else {
      return new MSK.com.gems.RollReasonsType.Builder(other);
    }
  }

  /**
   * RecordBuilder for RollReasonsType instances.
   */
  @org.apache.avro.specific.AvroGenerated
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<RollReasonsType>
    implements org.apache.avro.data.RecordBuilder<RollReasonsType> {

    private java.lang.String code;
    private java.lang.String reasonCategory;
    private java.lang.String reasonCustomerView;
    private java.lang.String reasonDescription;
    private java.lang.String reasonDetailsCustomerView;
    private java.lang.String routeUpdateReasonAction;
    private java.lang.String routeUpdateReasonDetails;
    private java.lang.String routeUpdateReasonTrigger;
    private java.lang.String updateTimeStamp;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(MSK.com.gems.RollReasonsType.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.code)) {
        this.code = data().deepCopy(fields()[0].schema(), other.code);
        fieldSetFlags()[0] = other.fieldSetFlags()[0];
      }
      if (isValidValue(fields()[1], other.reasonCategory)) {
        this.reasonCategory = data().deepCopy(fields()[1].schema(), other.reasonCategory);
        fieldSetFlags()[1] = other.fieldSetFlags()[1];
      }
      if (isValidValue(fields()[2], other.reasonCustomerView)) {
        this.reasonCustomerView = data().deepCopy(fields()[2].schema(), other.reasonCustomerView);
        fieldSetFlags()[2] = other.fieldSetFlags()[2];
      }
      if (isValidValue(fields()[3], other.reasonDescription)) {
        this.reasonDescription = data().deepCopy(fields()[3].schema(), other.reasonDescription);
        fieldSetFlags()[3] = other.fieldSetFlags()[3];
      }
      if (isValidValue(fields()[4], other.reasonDetailsCustomerView)) {
        this.reasonDetailsCustomerView = data().deepCopy(fields()[4].schema(), other.reasonDetailsCustomerView);
        fieldSetFlags()[4] = other.fieldSetFlags()[4];
      }
      if (isValidValue(fields()[5], other.routeUpdateReasonAction)) {
        this.routeUpdateReasonAction = data().deepCopy(fields()[5].schema(), other.routeUpdateReasonAction);
        fieldSetFlags()[5] = other.fieldSetFlags()[5];
      }
      if (isValidValue(fields()[6], other.routeUpdateReasonDetails)) {
        this.routeUpdateReasonDetails = data().deepCopy(fields()[6].schema(), other.routeUpdateReasonDetails);
        fieldSetFlags()[6] = other.fieldSetFlags()[6];
      }
      if (isValidValue(fields()[7], other.routeUpdateReasonTrigger)) {
        this.routeUpdateReasonTrigger = data().deepCopy(fields()[7].schema(), other.routeUpdateReasonTrigger);
        fieldSetFlags()[7] = other.fieldSetFlags()[7];
      }
      if (isValidValue(fields()[8], other.updateTimeStamp)) {
        this.updateTimeStamp = data().deepCopy(fields()[8].schema(), other.updateTimeStamp);
        fieldSetFlags()[8] = other.fieldSetFlags()[8];
      }
    }

    /**
     * Creates a Builder by copying an existing RollReasonsType instance
     * @param other The existing instance to copy.
     */
    private Builder(MSK.com.gems.RollReasonsType other) {
      super(SCHEMA$);
      if (isValidValue(fields()[0], other.code)) {
        this.code = data().deepCopy(fields()[0].schema(), other.code);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.reasonCategory)) {
        this.reasonCategory = data().deepCopy(fields()[1].schema(), other.reasonCategory);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.reasonCustomerView)) {
        this.reasonCustomerView = data().deepCopy(fields()[2].schema(), other.reasonCustomerView);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.reasonDescription)) {
        this.reasonDescription = data().deepCopy(fields()[3].schema(), other.reasonDescription);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.reasonDetailsCustomerView)) {
        this.reasonDetailsCustomerView = data().deepCopy(fields()[4].schema(), other.reasonDetailsCustomerView);
        fieldSetFlags()[4] = true;
      }
      if (isValidValue(fields()[5], other.routeUpdateReasonAction)) {
        this.routeUpdateReasonAction = data().deepCopy(fields()[5].schema(), other.routeUpdateReasonAction);
        fieldSetFlags()[5] = true;
      }
      if (isValidValue(fields()[6], other.routeUpdateReasonDetails)) {
        this.routeUpdateReasonDetails = data().deepCopy(fields()[6].schema(), other.routeUpdateReasonDetails);
        fieldSetFlags()[6] = true;
      }
      if (isValidValue(fields()[7], other.routeUpdateReasonTrigger)) {
        this.routeUpdateReasonTrigger = data().deepCopy(fields()[7].schema(), other.routeUpdateReasonTrigger);
        fieldSetFlags()[7] = true;
      }
      if (isValidValue(fields()[8], other.updateTimeStamp)) {
        this.updateTimeStamp = data().deepCopy(fields()[8].schema(), other.updateTimeStamp);
        fieldSetFlags()[8] = true;
      }
    }

    /**
      * Gets the value of the 'code' field.
      * @return The value.
      */
    public java.lang.String getCode() {
      return code;
    }


    /**
      * Sets the value of the 'code' field.
      * @param value The value of 'code'.
      * @return This builder.
      */
    public MSK.com.gems.RollReasonsType.Builder setCode(java.lang.String value) {
      validate(fields()[0], value);
      this.code = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'code' field has been set.
      * @return True if the 'code' field has been set, false otherwise.
      */
    public boolean hasCode() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'code' field.
      * @return This builder.
      */
    public MSK.com.gems.RollReasonsType.Builder clearCode() {
      code = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'reasonCategory' field.
      * @return The value.
      */
    public java.lang.String getReasonCategory() {
      return reasonCategory;
    }


    /**
      * Sets the value of the 'reasonCategory' field.
      * @param value The value of 'reasonCategory'.
      * @return This builder.
      */
    public MSK.com.gems.RollReasonsType.Builder setReasonCategory(java.lang.String value) {
      validate(fields()[1], value);
      this.reasonCategory = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'reasonCategory' field has been set.
      * @return True if the 'reasonCategory' field has been set, false otherwise.
      */
    public boolean hasReasonCategory() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'reasonCategory' field.
      * @return This builder.
      */
    public MSK.com.gems.RollReasonsType.Builder clearReasonCategory() {
      reasonCategory = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'reasonCustomerView' field.
      * @return The value.
      */
    public java.lang.String getReasonCustomerView() {
      return reasonCustomerView;
    }


    /**
      * Sets the value of the 'reasonCustomerView' field.
      * @param value The value of 'reasonCustomerView'.
      * @return This builder.
      */
    public MSK.com.gems.RollReasonsType.Builder setReasonCustomerView(java.lang.String value) {
      validate(fields()[2], value);
      this.reasonCustomerView = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'reasonCustomerView' field has been set.
      * @return True if the 'reasonCustomerView' field has been set, false otherwise.
      */
    public boolean hasReasonCustomerView() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'reasonCustomerView' field.
      * @return This builder.
      */
    public MSK.com.gems.RollReasonsType.Builder clearReasonCustomerView() {
      reasonCustomerView = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    /**
      * Gets the value of the 'reasonDescription' field.
      * @return The value.
      */
    public java.lang.String getReasonDescription() {
      return reasonDescription;
    }


    /**
      * Sets the value of the 'reasonDescription' field.
      * @param value The value of 'reasonDescription'.
      * @return This builder.
      */
    public MSK.com.gems.RollReasonsType.Builder setReasonDescription(java.lang.String value) {
      validate(fields()[3], value);
      this.reasonDescription = value;
      fieldSetFlags()[3] = true;
      return this;
    }

    /**
      * Checks whether the 'reasonDescription' field has been set.
      * @return True if the 'reasonDescription' field has been set, false otherwise.
      */
    public boolean hasReasonDescription() {
      return fieldSetFlags()[3];
    }


    /**
      * Clears the value of the 'reasonDescription' field.
      * @return This builder.
      */
    public MSK.com.gems.RollReasonsType.Builder clearReasonDescription() {
      reasonDescription = null;
      fieldSetFlags()[3] = false;
      return this;
    }

    /**
      * Gets the value of the 'reasonDetailsCustomerView' field.
      * @return The value.
      */
    public java.lang.String getReasonDetailsCustomerView() {
      return reasonDetailsCustomerView;
    }


    /**
      * Sets the value of the 'reasonDetailsCustomerView' field.
      * @param value The value of 'reasonDetailsCustomerView'.
      * @return This builder.
      */
    public MSK.com.gems.RollReasonsType.Builder setReasonDetailsCustomerView(java.lang.String value) {
      validate(fields()[4], value);
      this.reasonDetailsCustomerView = value;
      fieldSetFlags()[4] = true;
      return this;
    }

    /**
      * Checks whether the 'reasonDetailsCustomerView' field has been set.
      * @return True if the 'reasonDetailsCustomerView' field has been set, false otherwise.
      */
    public boolean hasReasonDetailsCustomerView() {
      return fieldSetFlags()[4];
    }


    /**
      * Clears the value of the 'reasonDetailsCustomerView' field.
      * @return This builder.
      */
    public MSK.com.gems.RollReasonsType.Builder clearReasonDetailsCustomerView() {
      reasonDetailsCustomerView = null;
      fieldSetFlags()[4] = false;
      return this;
    }

    /**
      * Gets the value of the 'routeUpdateReasonAction' field.
      * @return The value.
      */
    public java.lang.String getRouteUpdateReasonAction() {
      return routeUpdateReasonAction;
    }


    /**
      * Sets the value of the 'routeUpdateReasonAction' field.
      * @param value The value of 'routeUpdateReasonAction'.
      * @return This builder.
      */
    public MSK.com.gems.RollReasonsType.Builder setRouteUpdateReasonAction(java.lang.String value) {
      validate(fields()[5], value);
      this.routeUpdateReasonAction = value;
      fieldSetFlags()[5] = true;
      return this;
    }

    /**
      * Checks whether the 'routeUpdateReasonAction' field has been set.
      * @return True if the 'routeUpdateReasonAction' field has been set, false otherwise.
      */
    public boolean hasRouteUpdateReasonAction() {
      return fieldSetFlags()[5];
    }


    /**
      * Clears the value of the 'routeUpdateReasonAction' field.
      * @return This builder.
      */
    public MSK.com.gems.RollReasonsType.Builder clearRouteUpdateReasonAction() {
      routeUpdateReasonAction = null;
      fieldSetFlags()[5] = false;
      return this;
    }

    /**
      * Gets the value of the 'routeUpdateReasonDetails' field.
      * @return The value.
      */
    public java.lang.String getRouteUpdateReasonDetails() {
      return routeUpdateReasonDetails;
    }


    /**
      * Sets the value of the 'routeUpdateReasonDetails' field.
      * @param value The value of 'routeUpdateReasonDetails'.
      * @return This builder.
      */
    public MSK.com.gems.RollReasonsType.Builder setRouteUpdateReasonDetails(java.lang.String value) {
      validate(fields()[6], value);
      this.routeUpdateReasonDetails = value;
      fieldSetFlags()[6] = true;
      return this;
    }

    /**
      * Checks whether the 'routeUpdateReasonDetails' field has been set.
      * @return True if the 'routeUpdateReasonDetails' field has been set, false otherwise.
      */
    public boolean hasRouteUpdateReasonDetails() {
      return fieldSetFlags()[6];
    }


    /**
      * Clears the value of the 'routeUpdateReasonDetails' field.
      * @return This builder.
      */
    public MSK.com.gems.RollReasonsType.Builder clearRouteUpdateReasonDetails() {
      routeUpdateReasonDetails = null;
      fieldSetFlags()[6] = false;
      return this;
    }

    /**
      * Gets the value of the 'routeUpdateReasonTrigger' field.
      * @return The value.
      */
    public java.lang.String getRouteUpdateReasonTrigger() {
      return routeUpdateReasonTrigger;
    }


    /**
      * Sets the value of the 'routeUpdateReasonTrigger' field.
      * @param value The value of 'routeUpdateReasonTrigger'.
      * @return This builder.
      */
    public MSK.com.gems.RollReasonsType.Builder setRouteUpdateReasonTrigger(java.lang.String value) {
      validate(fields()[7], value);
      this.routeUpdateReasonTrigger = value;
      fieldSetFlags()[7] = true;
      return this;
    }

    /**
      * Checks whether the 'routeUpdateReasonTrigger' field has been set.
      * @return True if the 'routeUpdateReasonTrigger' field has been set, false otherwise.
      */
    public boolean hasRouteUpdateReasonTrigger() {
      return fieldSetFlags()[7];
    }


    /**
      * Clears the value of the 'routeUpdateReasonTrigger' field.
      * @return This builder.
      */
    public MSK.com.gems.RollReasonsType.Builder clearRouteUpdateReasonTrigger() {
      routeUpdateReasonTrigger = null;
      fieldSetFlags()[7] = false;
      return this;
    }

    /**
      * Gets the value of the 'updateTimeStamp' field.
      * @return The value.
      */
    public java.lang.String getUpdateTimeStamp() {
      return updateTimeStamp;
    }


    /**
      * Sets the value of the 'updateTimeStamp' field.
      * @param value The value of 'updateTimeStamp'.
      * @return This builder.
      */
    public MSK.com.gems.RollReasonsType.Builder setUpdateTimeStamp(java.lang.String value) {
      validate(fields()[8], value);
      this.updateTimeStamp = value;
      fieldSetFlags()[8] = true;
      return this;
    }

    /**
      * Checks whether the 'updateTimeStamp' field has been set.
      * @return True if the 'updateTimeStamp' field has been set, false otherwise.
      */
    public boolean hasUpdateTimeStamp() {
      return fieldSetFlags()[8];
    }


    /**
      * Clears the value of the 'updateTimeStamp' field.
      * @return This builder.
      */
    public MSK.com.gems.RollReasonsType.Builder clearUpdateTimeStamp() {
      updateTimeStamp = null;
      fieldSetFlags()[8] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public RollReasonsType build() {
      try {
        RollReasonsType record = new RollReasonsType();
        record.code = fieldSetFlags()[0] ? this.code : (java.lang.String) defaultValue(fields()[0]);
        record.reasonCategory = fieldSetFlags()[1] ? this.reasonCategory : (java.lang.String) defaultValue(fields()[1]);
        record.reasonCustomerView = fieldSetFlags()[2] ? this.reasonCustomerView : (java.lang.String) defaultValue(fields()[2]);
        record.reasonDescription = fieldSetFlags()[3] ? this.reasonDescription : (java.lang.String) defaultValue(fields()[3]);
        record.reasonDetailsCustomerView = fieldSetFlags()[4] ? this.reasonDetailsCustomerView : (java.lang.String) defaultValue(fields()[4]);
        record.routeUpdateReasonAction = fieldSetFlags()[5] ? this.routeUpdateReasonAction : (java.lang.String) defaultValue(fields()[5]);
        record.routeUpdateReasonDetails = fieldSetFlags()[6] ? this.routeUpdateReasonDetails : (java.lang.String) defaultValue(fields()[6]);
        record.routeUpdateReasonTrigger = fieldSetFlags()[7] ? this.routeUpdateReasonTrigger : (java.lang.String) defaultValue(fields()[7]);
        record.updateTimeStamp = fieldSetFlags()[8] ? this.updateTimeStamp : (java.lang.String) defaultValue(fields()[8]);
        return record;
      } catch (org.apache.avro.AvroMissingFieldException e) {
        throw e;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<RollReasonsType>
    WRITER$ = (org.apache.avro.io.DatumWriter<RollReasonsType>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<RollReasonsType>
    READER$ = (org.apache.avro.io.DatumReader<RollReasonsType>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

  @Override protected boolean hasCustomCoders() { return true; }

  @Override public void customEncode(org.apache.avro.io.Encoder out)
    throws java.io.IOException
  {
    if (this.code == null) {
      out.writeIndex(0);
      out.writeNull();
    } else {
      out.writeIndex(1);
      out.writeString(this.code);
    }

    if (this.reasonCategory == null) {
      out.writeIndex(0);
      out.writeNull();
    } else {
      out.writeIndex(1);
      out.writeString(this.reasonCategory);
    }

    if (this.reasonCustomerView == null) {
      out.writeIndex(0);
      out.writeNull();
    } else {
      out.writeIndex(1);
      out.writeString(this.reasonCustomerView);
    }

    if (this.reasonDescription == null) {
      out.writeIndex(0);
      out.writeNull();
    } else {
      out.writeIndex(1);
      out.writeString(this.reasonDescription);
    }

    if (this.reasonDetailsCustomerView == null) {
      out.writeIndex(0);
      out.writeNull();
    } else {
      out.writeIndex(1);
      out.writeString(this.reasonDetailsCustomerView);
    }

    if (this.routeUpdateReasonAction == null) {
      out.writeIndex(0);
      out.writeNull();
    } else {
      out.writeIndex(1);
      out.writeString(this.routeUpdateReasonAction);
    }

    if (this.routeUpdateReasonDetails == null) {
      out.writeIndex(0);
      out.writeNull();
    } else {
      out.writeIndex(1);
      out.writeString(this.routeUpdateReasonDetails);
    }

    if (this.routeUpdateReasonTrigger == null) {
      out.writeIndex(0);
      out.writeNull();
    } else {
      out.writeIndex(1);
      out.writeString(this.routeUpdateReasonTrigger);
    }

    if (this.updateTimeStamp == null) {
      out.writeIndex(0);
      out.writeNull();
    } else {
      out.writeIndex(1);
      out.writeString(this.updateTimeStamp);
    }

  }

  @Override public void customDecode(org.apache.avro.io.ResolvingDecoder in)
    throws java.io.IOException
  {
    org.apache.avro.Schema.Field[] fieldOrder = in.readFieldOrderIfDiff();
    if (fieldOrder == null) {
      if (in.readIndex() != 1) {
        in.readNull();
        this.code = null;
      } else {
        this.code = in.readString();
      }

      if (in.readIndex() != 1) {
        in.readNull();
        this.reasonCategory = null;
      } else {
        this.reasonCategory = in.readString();
      }

      if (in.readIndex() != 1) {
        in.readNull();
        this.reasonCustomerView = null;
      } else {
        this.reasonCustomerView = in.readString();
      }

      if (in.readIndex() != 1) {
        in.readNull();
        this.reasonDescription = null;
      } else {
        this.reasonDescription = in.readString();
      }

      if (in.readIndex() != 1) {
        in.readNull();
        this.reasonDetailsCustomerView = null;
      } else {
        this.reasonDetailsCustomerView = in.readString();
      }

      if (in.readIndex() != 1) {
        in.readNull();
        this.routeUpdateReasonAction = null;
      } else {
        this.routeUpdateReasonAction = in.readString();
      }

      if (in.readIndex() != 1) {
        in.readNull();
        this.routeUpdateReasonDetails = null;
      } else {
        this.routeUpdateReasonDetails = in.readString();
      }

      if (in.readIndex() != 1) {
        in.readNull();
        this.routeUpdateReasonTrigger = null;
      } else {
        this.routeUpdateReasonTrigger = in.readString();
      }

      if (in.readIndex() != 1) {
        in.readNull();
        this.updateTimeStamp = null;
      } else {
        this.updateTimeStamp = in.readString();
      }

    } else {
      for (int i = 0; i < 9; i++) {
        switch (fieldOrder[i].pos()) {
        case 0:
          if (in.readIndex() != 1) {
            in.readNull();
            this.code = null;
          } else {
            this.code = in.readString();
          }
          break;

        case 1:
          if (in.readIndex() != 1) {
            in.readNull();
            this.reasonCategory = null;
          } else {
            this.reasonCategory = in.readString();
          }
          break;

        case 2:
          if (in.readIndex() != 1) {
            in.readNull();
            this.reasonCustomerView = null;
          } else {
            this.reasonCustomerView = in.readString();
          }
          break;

        case 3:
          if (in.readIndex() != 1) {
            in.readNull();
            this.reasonDescription = null;
          } else {
            this.reasonDescription = in.readString();
          }
          break;

        case 4:
          if (in.readIndex() != 1) {
            in.readNull();
            this.reasonDetailsCustomerView = null;
          } else {
            this.reasonDetailsCustomerView = in.readString();
          }
          break;

        case 5:
          if (in.readIndex() != 1) {
            in.readNull();
            this.routeUpdateReasonAction = null;
          } else {
            this.routeUpdateReasonAction = in.readString();
          }
          break;

        case 6:
          if (in.readIndex() != 1) {
            in.readNull();
            this.routeUpdateReasonDetails = null;
          } else {
            this.routeUpdateReasonDetails = in.readString();
          }
          break;

        case 7:
          if (in.readIndex() != 1) {
            in.readNull();
            this.routeUpdateReasonTrigger = null;
          } else {
            this.routeUpdateReasonTrigger = in.readString();
          }
          break;

        case 8:
          if (in.readIndex() != 1) {
            in.readNull();
            this.updateTimeStamp = null;
          } else {
            this.updateTimeStamp = in.readString();
          }
          break;

        default:
          throw new java.io.IOException("Corrupt ResolvingDecoder.");
        }
      }
    }
  }
}










