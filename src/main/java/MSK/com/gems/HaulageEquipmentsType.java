/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package MSK.com.gems;

import org.apache.avro.generic.GenericArray;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.util.Utf8;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@org.apache.avro.specific.AvroGenerated
public class HaulageEquipmentsType extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = 2767008322759143350L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"HaulageEquipmentsType\",\"namespace\":\"MSK.com.gems\",\"fields\":[{\"name\":\"haulageEquipment\",\"type\":[\"null\",{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"HaulageEquipmentType\",\"fields\":[{\"name\":\"createTimestamp\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"haulageArrangeEquipment\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"haulageArrangementDepot\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"haulageSiteArrive\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"haulageSiteDepart\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"haulageSiteVisit\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"sequenceInEquipment\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"timingArriveFromLocal\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"timingArriveToLocal\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"timingDepartFromLocal\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"timingDepartToLocal\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"truckerPreference\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"updateTimeStamp\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]}]}}]}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<HaulageEquipmentsType> ENCODER =
      new BinaryMessageEncoder<HaulageEquipmentsType>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<HaulageEquipmentsType> DECODER =
      new BinaryMessageDecoder<HaulageEquipmentsType>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageEncoder instance used by this class.
   * @return the message encoder used by this class
   */
  public static BinaryMessageEncoder<HaulageEquipmentsType> getEncoder() {
    return ENCODER;
  }

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   * @return the message decoder used by this class
   */
  public static BinaryMessageDecoder<HaulageEquipmentsType> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   * @return a BinaryMessageDecoder instance for this class backed by the given SchemaStore
   */
  public static BinaryMessageDecoder<HaulageEquipmentsType> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<HaulageEquipmentsType>(MODEL$, SCHEMA$, resolver);
  }

  /**
   * Serializes this HaulageEquipmentsType to a ByteBuffer.
   * @return a buffer holding the serialized data for this instance
   * @throws java.io.IOException if this instance could not be serialized
   */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /**
   * Deserializes a HaulageEquipmentsType from a ByteBuffer.
   * @param b a byte buffer holding serialized data for an instance of this class
   * @return a HaulageEquipmentsType instance decoded from the given buffer
   * @throws java.io.IOException if the given bytes could not be deserialized into an instance of this class
   */
  public static HaulageEquipmentsType fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

   private java.util.List<MSK.com.gems.HaulageEquipmentType> haulageEquipment;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public HaulageEquipmentsType() {}

  /**
   * All-args constructor.
   * @param haulageEquipment The new value for haulageEquipment
   */
  public HaulageEquipmentsType(java.util.List<MSK.com.gems.HaulageEquipmentType> haulageEquipment) {
    this.haulageEquipment = haulageEquipment;
  }

  public org.apache.avro.specific.SpecificData getSpecificData() { return MODEL$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return haulageEquipment;
    default: throw new IndexOutOfBoundsException("Invalid index: " + field$);
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: haulageEquipment = (java.util.List<MSK.com.gems.HaulageEquipmentType>)value$; break;
    default: throw new IndexOutOfBoundsException("Invalid index: " + field$);
    }
  }

  /**
   * Gets the value of the 'haulageEquipment' field.
   * @return The value of the 'haulageEquipment' field.
   */
  public java.util.List<MSK.com.gems.HaulageEquipmentType> getHaulageEquipment() {
    return haulageEquipment;
  }


  /**
   * Sets the value of the 'haulageEquipment' field.
   * @param value the value to set.
   */
  public void setHaulageEquipment(java.util.List<MSK.com.gems.HaulageEquipmentType> value) {
    this.haulageEquipment = value;
  }

  /**
   * Creates a new HaulageEquipmentsType RecordBuilder.
   * @return A new HaulageEquipmentsType RecordBuilder
   */
  public static MSK.com.gems.HaulageEquipmentsType.Builder newBuilder() {
    return new MSK.com.gems.HaulageEquipmentsType.Builder();
  }

  /**
   * Creates a new HaulageEquipmentsType RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new HaulageEquipmentsType RecordBuilder
   */
  public static MSK.com.gems.HaulageEquipmentsType.Builder newBuilder(MSK.com.gems.HaulageEquipmentsType.Builder other) {
    if (other == null) {
      return new MSK.com.gems.HaulageEquipmentsType.Builder();
    } else {
      return new MSK.com.gems.HaulageEquipmentsType.Builder(other);
    }
  }

  /**
   * Creates a new HaulageEquipmentsType RecordBuilder by copying an existing HaulageEquipmentsType instance.
   * @param other The existing instance to copy.
   * @return A new HaulageEquipmentsType RecordBuilder
   */
  public static MSK.com.gems.HaulageEquipmentsType.Builder newBuilder(MSK.com.gems.HaulageEquipmentsType other) {
    if (other == null) {
      return new MSK.com.gems.HaulageEquipmentsType.Builder();
    } else {
      return new MSK.com.gems.HaulageEquipmentsType.Builder(other);
    }
  }

  /**
   * RecordBuilder for HaulageEquipmentsType instances.
   */
  @org.apache.avro.specific.AvroGenerated
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<HaulageEquipmentsType>
    implements org.apache.avro.data.RecordBuilder<HaulageEquipmentsType> {

    private java.util.List<MSK.com.gems.HaulageEquipmentType> haulageEquipment;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(MSK.com.gems.HaulageEquipmentsType.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.haulageEquipment)) {
        this.haulageEquipment = data().deepCopy(fields()[0].schema(), other.haulageEquipment);
        fieldSetFlags()[0] = other.fieldSetFlags()[0];
      }
    }

    /**
     * Creates a Builder by copying an existing HaulageEquipmentsType instance
     * @param other The existing instance to copy.
     */
    private Builder(MSK.com.gems.HaulageEquipmentsType other) {
      super(SCHEMA$);
      if (isValidValue(fields()[0], other.haulageEquipment)) {
        this.haulageEquipment = data().deepCopy(fields()[0].schema(), other.haulageEquipment);
        fieldSetFlags()[0] = true;
      }
    }

    /**
      * Gets the value of the 'haulageEquipment' field.
      * @return The value.
      */
    public java.util.List<MSK.com.gems.HaulageEquipmentType> getHaulageEquipment() {
      return haulageEquipment;
    }


    /**
      * Sets the value of the 'haulageEquipment' field.
      * @param value The value of 'haulageEquipment'.
      * @return This builder.
      */
    public MSK.com.gems.HaulageEquipmentsType.Builder setHaulageEquipment(java.util.List<MSK.com.gems.HaulageEquipmentType> value) {
      validate(fields()[0], value);
      this.haulageEquipment = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'haulageEquipment' field has been set.
      * @return True if the 'haulageEquipment' field has been set, false otherwise.
      */
    public boolean hasHaulageEquipment() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'haulageEquipment' field.
      * @return This builder.
      */
    public MSK.com.gems.HaulageEquipmentsType.Builder clearHaulageEquipment() {
      haulageEquipment = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public HaulageEquipmentsType build() {
      try {
        HaulageEquipmentsType record = new HaulageEquipmentsType();
        record.haulageEquipment = fieldSetFlags()[0] ? this.haulageEquipment : (java.util.List<MSK.com.gems.HaulageEquipmentType>) defaultValue(fields()[0]);
        return record;
      } catch (org.apache.avro.AvroMissingFieldException e) {
        throw e;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<HaulageEquipmentsType>
    WRITER$ = (org.apache.avro.io.DatumWriter<HaulageEquipmentsType>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<HaulageEquipmentsType>
    READER$ = (org.apache.avro.io.DatumReader<HaulageEquipmentsType>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

  @Override protected boolean hasCustomCoders() { return true; }

  @Override public void customEncode(org.apache.avro.io.Encoder out)
    throws java.io.IOException
  {
    if (this.haulageEquipment == null) {
      out.writeIndex(0);
      out.writeNull();
    } else {
      out.writeIndex(1);
      long size0 = this.haulageEquipment.size();
      out.writeArrayStart();
      out.setItemCount(size0);
      long actualSize0 = 0;
      for (MSK.com.gems.HaulageEquipmentType e0: this.haulageEquipment) {
        actualSize0++;
        out.startItem();
        e0.customEncode(out);
      }
      out.writeArrayEnd();
      if (actualSize0 != size0)
        throw new java.util.ConcurrentModificationException("Array-size written was " + size0 + ", but element count was " + actualSize0 + ".");
    }

  }

  @Override public void customDecode(org.apache.avro.io.ResolvingDecoder in)
    throws java.io.IOException
  {
    org.apache.avro.Schema.Field[] fieldOrder = in.readFieldOrderIfDiff();
    if (fieldOrder == null) {
      if (in.readIndex() != 1) {
        in.readNull();
        this.haulageEquipment = null;
      } else {
        long size0 = in.readArrayStart();
        java.util.List<MSK.com.gems.HaulageEquipmentType> a0 = this.haulageEquipment;
        if (a0 == null) {
          a0 = new SpecificData.Array<MSK.com.gems.HaulageEquipmentType>((int)size0, SCHEMA$.getField("haulageEquipment").schema().getTypes().get(1));
          this.haulageEquipment = a0;
        } else a0.clear();
        SpecificData.Array<MSK.com.gems.HaulageEquipmentType> ga0 = (a0 instanceof SpecificData.Array ? (SpecificData.Array<MSK.com.gems.HaulageEquipmentType>)a0 : null);
        for ( ; 0 < size0; size0 = in.arrayNext()) {
          for ( ; size0 != 0; size0--) {
            MSK.com.gems.HaulageEquipmentType e0 = (ga0 != null ? ga0.peek() : null);
            if (e0 == null) {
              e0 = new MSK.com.gems.HaulageEquipmentType();
            }
            e0.customDecode(in);
            a0.add(e0);
          }
        }
      }

    } else {
      for (int i = 0; i < 1; i++) {
        switch (fieldOrder[i].pos()) {
        case 0:
          if (in.readIndex() != 1) {
            in.readNull();
            this.haulageEquipment = null;
          } else {
            long size0 = in.readArrayStart();
            java.util.List<MSK.com.gems.HaulageEquipmentType> a0 = this.haulageEquipment;
            if (a0 == null) {
              a0 = new SpecificData.Array<MSK.com.gems.HaulageEquipmentType>((int)size0, SCHEMA$.getField("haulageEquipment").schema().getTypes().get(1));
              this.haulageEquipment = a0;
            } else a0.clear();
            SpecificData.Array<MSK.com.gems.HaulageEquipmentType> ga0 = (a0 instanceof SpecificData.Array ? (SpecificData.Array<MSK.com.gems.HaulageEquipmentType>)a0 : null);
            for ( ; 0 < size0; size0 = in.arrayNext()) {
              for ( ; size0 != 0; size0--) {
                MSK.com.gems.HaulageEquipmentType e0 = (ga0 != null ? ga0.peek() : null);
                if (e0 == null) {
                  e0 = new MSK.com.gems.HaulageEquipmentType();
                }
                e0.customDecode(in);
                a0.add(e0);
              }
            }
          }
          break;

        default:
          throw new java.io.IOException("Corrupt ResolvingDecoder.");
        }
      }
    }
  }
}










