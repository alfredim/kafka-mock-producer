package com.example.kafka_mock_producer.route;

import com.example.kafka_mock_producer.processor.KafkaProducerRequestProcessor;
import com.example.kafka_mock_producer.processor.KafkaProducerResponseProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class KafkaProducerRoute extends RouteBuilder {

    public static final String KAFKA_PRODUCER_ROUTE = "direct:kafka-producer-route";
    public static final String KAFKA_PRODUCER_ID = "kafka-producer-route";

    private final KafkaProducerRequestProcessor kafkaProducerRequestProcessor;
    private final KafkaProducerResponseProcessor kafkaProducerResponseProcessor;

    @Autowired
    public KafkaProducerRoute(KafkaProducerRequestProcessor kafkaProducerRequestProcessor, KafkaProducerResponseProcessor kafkaProducerResponseProcessor) {
        this.kafkaProducerRequestProcessor = kafkaProducerRequestProcessor;
        this.kafkaProducerResponseProcessor = kafkaProducerResponseProcessor;
    }

    @Override
    public void configure() throws Exception {
        from(KAFKA_PRODUCER_ROUTE)
                .routeId(KAFKA_PRODUCER_ID)
                .process(kafkaProducerRequestProcessor)
                .to("kafka:MSK.shipment.externalDcsaEvents.topic.internal.any.v1")
                .process(kafkaProducerResponseProcessor);
    }
}
