package com.example.kafka_mock_producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages = {"com.example.kafka_mock_producer.config", "com.example.kafka_mock_producer"})
public class KafkaMockProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaMockProducerApplication.class, args);
	}
}
