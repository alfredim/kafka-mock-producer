package com.example.kafka_mock_producer.processor;

import MSK.com.external.dcsa.BadKey;
import MSK.com.external.dcsa.DcsaTrackTraceEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.kafka.KafkaConstants;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class KafkaProducerRequestProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        log.info("Kafka request processor");
        var dcsaTrackAndTraceToBeStored = exchange.getIn().getBody(DcsaTrackTraceEvent.class);
        log.info("Sending message: {}", dcsaTrackAndTraceToBeStored);

        if ("true".equals(String.valueOf(exchange.getIn().getHeader("BAD_KEY")))) {
            exchange.getIn().setHeader(KafkaConstants.KEY, BadKey.MAEU);
        }
        exchange.getIn().setBody(dcsaTrackAndTraceToBeStored);
    }
}
