package com.example.kafka_mock_producer.processor;

import com.example.kafka_mock_producer.model.Status;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class KafkaProducerResponseProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        log.info("Kafka response processor");
        exchange.getIn().setBody(Status.builder().status("Success").build());
    }
}
