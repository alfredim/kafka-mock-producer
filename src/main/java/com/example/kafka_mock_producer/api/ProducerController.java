package com.example.kafka_mock_producer.api;

import MSK.com.external.dcsa.DcsaTrackTraceEvent;
import com.example.kafka_mock_producer.model.Status;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestOperationParamDefinition;
import org.apache.camel.model.rest.RestParamType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static com.example.kafka_mock_producer.route.KafkaProducerRoute.KAFKA_PRODUCER_ROUTE;
import static org.apache.camel.model.rest.RestBindingMode.json;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@Component
public class ProducerController extends RouteBuilder {

    private static final RestOperationParamDefinition BAD_KEY = new RestOperationParamDefinition()
            .name("BAD_KEY")
            .type(RestParamType.header)
            .dataType("boolean")
            .defaultValue("false")
            .description("Produce a error in kafka connect")
            .required(false);

    @Value("#{new Integer('${server.port:8080}')}")
    private int serverPort;

    @Override
    public void configure() throws Exception {

        restConfiguration()
                .apiHost("localhost")
                .port(serverPort)
                .contextPath("/kafka-producer-service")
                .apiContextPath("/")
                .apiProperty("api.title", "Test REST API")
                .apiProperty("api.version", "v1")
                .apiContextRouteId("doc-api")
                .bindingMode(json);

        rest()
                .tag("Kafka Producer API")
                .consumes(APPLICATION_JSON_VALUE)
                .produces(APPLICATION_JSON_VALUE)
                .bindingMode(json)
                .description("something")

                .get("/test")
                .outType(Status.class)
                .route()
                    .routeId("hello-world-route")
                    .process(exchange -> {
                        exchange.getIn().setBody(Status.builder().status("OK").build());
                    })
                    .log("${body}")
                .endRest()

                .post("/mock-dcsa")
                .type(DcsaTrackTraceEvent.class)
                .outType(Status.class)
                .param(BAD_KEY)
                .route()
                    .to(KAFKA_PRODUCER_ROUTE)
                .endRest();

    }
}
