package com.example.kafka_mock_producer.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Status {

    private String status;
}
